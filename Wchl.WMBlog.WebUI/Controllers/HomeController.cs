﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wchl.WMBlog.Common.ToolsHelper;
using Wchl.WMBlog.IServices;
using Wchl.WMBlog.Model.Models;
using Wchl.WMBlog.Model.VeiwModels;
using Wchl.WMBlog.Services;
using Wchl.WMBlog.WebCore;
using Wchl.WMBlog.WebCore.Attrs;
using Webdiyer.WebControls.Mvc;

namespace Wchl.WMBlog.WebUI.Controllers
{
    [SkipCheckLogin]
    public class HomeController : BaseController
    {
        IsysUserInfoServices userinfoservice;
        IBlogArticleServices BlogArticleServive;
        IAdvertisementServices AdvertisementServices;
        IGuestbookServices GuestbookServices;

        public HomeController(IsysUserInfoServices userinfs, IBlogArticleServices BlogArticleServive, IAdvertisementServices AdvertisementServices, IGuestbookServices GuestbookServices)
        {
            this.userinfoservice = userinfs;
            this.BlogArticleServive = BlogArticleServive;
            this.AdvertisementServices = AdvertisementServices;
            this.GuestbookServices = GuestbookServices;
        }


        public ActionResult Index(int pageindex = 1)
        {
            //获取控制器名称
            ViewBag.controllername = RouteData.Values["controller"].ToString().ToLower();

            int pagesize = 6;
            //获取发布博文信息
            var blogArticleList = BlogArticleServive.QueryWhere(a => true).OrderByDescending(a => a.bCreateTime).ToPagedList(pageindex, pagesize);
            foreach (var item in blogArticleList)
            {
                if (!string.IsNullOrEmpty(item.bcontent))
                {
                    item.bcontent = Tools.ReplaceHtmlTag(item.bcontent);
                    if (item.bcontent.Length > 200)
                    {
                        item.bcontent = item.bcontent.Substring(0, 200);
                    }
                }
                
            }
            //获取轮播广告新
            ViewBag.adList = AdvertisementServices.QueryOrderBy(a => true, a => a.Createdate, false).ToPagedList(1, 3);
            //发布时间排序
            ViewBag.blogtimelist = BlogArticleServive.QueryOrderBy(c => true, c => c.bCreateTime, false);
            //评论排序
            ViewBag.blogtrafficlist = BlogArticleServive.QueryOrderBy(c => true, c => c.btraffic, false);
            //留言排序
            string sql = @"select a.*,b.btitle from (select blogId,count(1) as counts  from Guestbook group by blogId) as a
inner join BlogArticle as b
on
b.bID=a.blogId order by counts desc";

            ViewBag.blogguestbooklist = GuestbookServices.RunProc<TopgbViewModels>(sql);
           

            return View(blogArticleList);
        }
    }
}