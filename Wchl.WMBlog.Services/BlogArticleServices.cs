﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wchl.WMBlog.Common.ToolsHelper;
using Wchl.WMBlog.IRepository;
using Wchl.WMBlog.IServices;
using Wchl.WMBlog.Model.Models;
using Wchl.WMBlog.Model.VeiwModels;
using Wchl.WMBlog.Services.Base;

namespace Wchl.WMBlog.Services
{
    public class BlogArticleServices: BaseServices<BlogArticle>, IBlogArticleServices
    {
        IBlogArticleRepository dal;

        public BlogArticleServices(IBlogArticleRepository dal)
        {
            this.dal = dal;
            base.baseDal = dal;
        }

        /// <summary>
        /// 获取视图博客详情信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public BlogViewModels getBlogDetails(int id)
        {
            BlogArticle blogArticle = dal.QueryWhere(a => a.bID == id).FirstOrDefault();
            BlogArticle nextblog= dal.QueryWhere(a => a.bID == id-1).FirstOrDefault();
            BlogArticle prevblog = dal.QueryWhere(a => a.bID == id+1).FirstOrDefault();
            blogArticle.btraffic += 1;
            dal.Edit(blogArticle, new string[] { "btraffic" });
            dal.SaverChanges();
            //AutoMapper自动映射
            Mapper.Initialize(cfg => cfg.CreateMap<BlogArticle, BlogViewModels>());
            BlogViewModels models = Mapper.Map<BlogArticle, BlogViewModels>(blogArticle);
            if (nextblog!=null)
            {
                models.next = nextblog.btitle;
                models.nextID = nextblog.bID;
            }

            if (prevblog != null)
            {
                models.previous = prevblog.btitle;
                models.previousID = prevblog.bID;
            }
            models.digest = Tools.ReplaceHtmlTag(blogArticle.bcontent).Length > 100 ? Tools.ReplaceHtmlTag(blogArticle.bcontent).Substring(0, 200) : Tools.ReplaceHtmlTag(blogArticle.bcontent);
            return models;
        
        }

        
    }
}
